import os
from lib.q_processor import q_processor
from argparse import ArgumentParser

def main():
  parser = ArgumentParser()
  parser.add_argument("-f", "--file", dest="filename",
                  help="Input file", metavar="FILE")

  args = parser.parse_args()

  if args.filename == None:
    print("Add a file path argument using --file")
  elif args.filename != None and os.path.isfile(args.filename) == False:
    print("The input file {} does not exist".format(args.filename))
  else:
    f = open(args.filename,"r")
    if f.mode == 'r':
      try:
        fl = f.readlines() 
        p = q_processor()
        p.process_list(fl)
      except Exception as em:
        print("An error accured while processing the input file: {} ".format(em))
  

if __name__ == "__main__":
  main()
from urllib import request, parse
import json
from .q_logger import q_logger
import configparser

class q_slack():

    url = None
    SendMessages = None

    def __init__(self, SendMessages, url, debug_mode):
        self.url = url
        self.SendMessages = SendMessages
        self.logger = q_logger(debug_mode)


    async def send_message_to_slack(self,text):
        if self.SendMessages == False:
            return
        post = {"text": "{0}".format(text)}

        try:
            json_data = json.dumps(post)
            req = request.Request(self.url,
                                data=json_data.encode('ascii'),
                                headers={'Content-Type': 'application/json'}) 
            resp = request.urlopen(req)
        except Exception as em:
            self.logger.log_error("Slack error: {}".format(em))
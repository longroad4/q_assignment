import re
from .q_logger import q_logger
from datetime import datetime
import json

class q_validator():

    def __init__(self, debug_mode):
        self.logger = q_logger(debug_mode)

    def validate(self, data):
        
        try:
            theJSON = json.loads(str(data))
        except ValueError as e:
            self.logger.log_info("The line is not a valid json string: {}  ".format(e))
            return False

        try:
            isIter = iter(theJSON)
        except TypeError as e:
            self.logger.log_info("The line is not a valid json string: {}  ".format(e))
            return False

        if "event_date" not in theJSON:
            self.logger.log_info("Missing required field event_date ")
            return False
        
        if "event_name" not in theJSON:
            self.logger.log_info("Missing required field event_name ")
            return False

        if "product_version" in theJSON and not re.match('^([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?$',theJSON["product_version"]):
            self.logger.log_info("Invalid field product_version ")
            return False

        if "user_agent" in theJSON and not re.match('.+?[/\s][\d.]+',theJSON["user_agent"]):
            self.logger.log_info("Invalid field user_agent ")
            return False
        
        try:
            event_date = datetime.strptime(theJSON["event_date"], "%Y-%m-%d'T'%H:%M:%S.%f%z")
        except ValueError as e:
            self.logger.log_info("Invalid field event_date ")
            return False
        
        return True
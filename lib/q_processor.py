import json
from .q_logger import q_logger
from .q_validator import q_validator
from .q_slack import q_slack
from datetime import datetime
from ua_parser import user_agent_parser
import configparser
import asyncio

class q_processor():
    cur_new_user_date = None
    cur_old_user_date = None
    cur_mobile_date = None
    ctr_all = 0
    ctr_new = 0
    ctr_old = 0
    ctr_mob = 0
    ctr_uni = 0
    ctr_err = 0
    ctr_oth = 0
    time_processing = 0
    date_format = "%Y-%m-%d'T'%H:%M:%S.%f%z"
    slackList = []

    slackURL = None
    SendMessages = None
    debug_mode = None

    def __init__(self, debug_mode = None, SendMessages = None, slackURL = None ):
        config = configparser.ConfigParser()
        config.read('lib/config.ini')
        if debug_mode != None:
            self.debug_mode = debug_mode
        else:
            if 'DEFAULT' in config and 'DebugMode' in config['DEFAULT']:
                self.debug_mode = config['DEFAULT'].getboolean('DebugMode')

        if slackURL != None:
            self.slackURL = slackURL
        else:
            if 'slack.com' in config and 'url' in config['slack.com']:
                self.slackURL = config['slack.com']['url']

        if SendMessages != None:
            self.SendMessages = SendMessages
        else:
            if 'slack.com' in config and 'SendMessages' in config['slack.com']:
                self.SendMessages = config['slack.com'].getboolean('SendMessages')

        self.logger = q_logger(self.debug_mode)
        self.slacker = q_slack(self.SendMessages, self.slackURL, self.debug_mode)
        self.validator = q_validator(self.debug_mode)


    def process_list(self, list):
        self.ctr_all = len(list)
        start_date = datetime.now()

        for idx,item in enumerate(list):
            self.logger.log_info("Processing line # {} ".format(idx + 1))
            self.process_line(item)

        end_date = datetime.now()
        diff = (end_date - start_date )
        self.time_processing = diff.seconds

        self.showResults()

        if len(self.slackList) > 0:
            print("Sending {} messages to slack ".format(len(self.slackList)))
            self.logger.log_info("Sending {} messages to slack ".format(len(self.slackList)))
            try:
                self.loop = asyncio.new_event_loop()
                self.loop.run_until_complete(self.sendSlackMessages())
            except Exception as e:
                self.loop.close()
    
    def process_line(self, data):
        try:
            
            if self.validator.validate(data) == False:
                self.ctr_err = self.ctr_err + 1
            else: 
                theJSON = json.loads(data)
                
                # The rules only deal with two specific event names, so we skip other names
                if theJSON["event_name"] not in ["uninstall", "user_login"]:
                    self.ctr_oth = self.ctr_oth + 1
                    return

                event_new = False
                event_old = False

                if self.ruleNewProductUser(theJSON):
                    event_new = True
                    self.ctr_new = self.ctr_new + 1
                
                if event_new == False and self.ruleOldProductUser(theJSON):
                    event_old = True
                    self.ctr_old = self.ctr_old + 1
                
                if event_new == False and event_old == False and self.ruleProductUnInstall(theJSON):
                    self.ctr_uni = self.ctr_uni + 1
                
                if event_new == True and self.ruleNewProductMobile(theJSON):
                    self.ctr_mob = self.ctr_mob + 1

        except Exception as e:
            self.logger.log_info("An error accured while checking the rules: {}  ".format(e))

    def ruleNewProductUser(self, theJSON):
        if "product_version" in theJSON and theJSON["product_version"] >= "2.0.0" and  theJSON["event_name"] == "user_login":
            self.logger.log_info("new_product_user event_date {} ".format(theJSON["event_date"]))

            if self.SendMessages == True:
                if self.cur_new_user_date == None: # first new_product_user event
                    self.cur_new_user_date = theJSON["event_date"]
                    # Send slack
                    self.slackList.append("new_product_user: \n {}".format(theJSON))
                elif self.isTimeToSlack(self.cur_new_user_date, theJSON["event_date"], 1):
                    self.cur_new_user_date = theJSON["event_date"]
                    # Send slack
                    self.slackList.append("new_product_user: \n {}".format(theJSON)) 

            return True

        return False

    def ruleOldProductUser(self, theJSON):
        if "product_version" in theJSON and theJSON["product_version"] < "2.0.0" and  theJSON["event_name"] == "user_login":
            self.logger.log_info("old_product_user event_date {} ".format(theJSON["event_date"]))

            if self.SendMessages == True:
                if self.cur_old_user_date == None:
                    self.cur_old_user_date = theJSON["event_date"]
                    # Send slack
                    self.slackList.append("old_product_user: \n {}".format(theJSON))
                elif self.isTimeToSlack(self.cur_old_user_date, theJSON["event_date"], 24):
                    self.cur_old_user_date = theJSON["event_date"]
                    # Send slack
                    self.slackList.append("old_product_user: \n {}".format(theJSON))          
            return True
            
        return False

    def ruleProductUnInstall(self, theJSON):
        if theJSON["event_name"] == "uninstall":
            self.logger.log_info("product_uninstall event_date {} ".format(theJSON["event_date"]))
            # Send slack
            if self.SendMessages == True:
                self.slackList.append("product_uninstall: \n {}".format(theJSON)) 
            return True

        return False

    def ruleNewProductMobile(self, theJSON):
        if "product_version" in theJSON and theJSON["product_version"] >= "2.0.0" and  theJSON["event_name"] == "user_login":
            if "user_agent" in theJSON and self.isMobileUser(theJSON["user_agent"]):
                self.logger.log_info("new_product_mobile_user event_date {} ".format(theJSON["event_date"]))

                if self.SendMessages == True:
                    if self.cur_mobile_date == None:
                        self.cur_mobile_date = theJSON["event_date"]
                        # Send slack
                        self.slackList.append("new_product_mobile_user: \n {}".format(theJSON)) 
                    elif self.isTimeToSlack(self.cur_mobile_date, theJSON["event_date"], 1):
                        self.cur_mobile_date = theJSON["event_date"]
                        # Send slack
                        self.slackList.append("new_product_mobile_user: \n {}".format(theJSON))  

                return True
                
        return False

    def isMobileUser(self, user_agent):
        parsed_device_string = user_agent_parser.ParseDevice(user_agent)
        if("brand" in parsed_device_string and "family" in parsed_device_string and "model" in parsed_device_string ):
            if parsed_device_string["brand"] == None and parsed_device_string["family"] == 'Other' and parsed_device_string["model"] == None:
                return False
            else:
                return True
        return False

    def isTimeToSlack(self, old_date, new_date, hours):
        compare_da = datetime.strptime(old_date, self.date_format)
        event_date = datetime.strptime(new_date, self.date_format)
        if event_date > compare_da:
            diff = event_date - compare_da
            if diff.total_seconds()/3600 >= int(hours):
                return True
        return False

    async def sendSlackMessages(self):
        for item in self.slackList:
            s = self.loop.create_task(self.slacker.send_message_to_slack(item) ) 

    def showResults(self):
        print("# items processed: {}".format(self.ctr_all))
        print("# new_product_user: {}".format(self.ctr_new))
        print("# old_product_user: {}".format(self.ctr_old))
        print("# new_product_mobile_user: {}".format(self.ctr_mob))
        print("# product_uninstall: {}".format(self.ctr_uni))
        print("# Invalid items: {}".format(self.ctr_err))
        if self.ctr_oth > 0:
            print("# other event names: {}".format(self.ctr_err))
        print("# Seconds processing: {}".format(self.time_processing))
        if self.time_processing > 0:
            print("# Records per second: {}".format(self.ctr_all/self.time_processing))

        self.logger.log_info("# Items processed: {}".format(self.ctr_all))
        self.logger.log_info("# new_product_user: {}".format(self.ctr_new))
        self.logger.log_info("# old_product_user: {}".format(self.ctr_old))
        self.logger.log_info("# new_product_mobile_user: {}".format(self.ctr_mob))
        self.logger.log_info("# product_uninstall: {}".format(self.ctr_uni))
        self.logger.log_info("# Invalid items: {}".format(self.ctr_err))
        if self.ctr_oth > 0:
            self.logger.log_info("# other event names: {}".format(self.ctr_err))
        self.logger.log_info("# Seconds processing: {}".format(self.time_processing))
        if self.time_processing > 0:
            self.logger.log_info("# Records per second: {}".format(self.ctr_all/self.time_processing))


    
    
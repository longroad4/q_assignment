import logging

class q_logger():

    def __init__(self, debug_mode = True):
        if debug_mode == True:
            debug_level = logging.DEBUG
        else:
            debug_level = logging.ERROR
        fmtStr = "%(asctime)s: %(levelname)s: %(funcName)s %(message)s"
        dateStr = "%m/%d/%Y %I:%M:%S %p"
        logging.basicConfig(filename="lib/output.log",
                            level=debug_level,
                            format=fmtStr,
                            filemode="w",
                            datefmt=dateStr)

    def log_debug(self, msg):
        logging.debug(msg)

    def log_info(self, msg):
        logging.info(msg)

    def log_warning(self, msg):
        logging.warning(msg)

    def log_error(self, msg):
        logging.error(msg)

    def log_critical(self, msg):
        logging.critical(msg)
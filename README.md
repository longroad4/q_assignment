# q_assignment

String Processor Assignment:

Created with Python 3.7. 

Code:
- git clone https://longroad4@bitbucket.org/longroad4/q_assignment.git

Requirements:
 - pip install -r requirements.txt

To run from the command line:
 - python assignment.py --file="input.txt"

Slack integration:
    I used "Incoming Webhooks" by following: https://api.slack.com/incoming-webhooks, copy the url to the file lib/config.ini

Config file:
    lib/config.ini
    - DebugMode: if True info messages will be written to file lib/output.log
    - Url: Slack web hood URL
    - SendMessages: if True messages will be send to the Slack channel

Running the tests:
- python test_q_processor.py
- python test_q_validator.py

To test using Docker hub image:
- docker pull longroad4/q_assignment:latest
- docker container run -it longroad4/q_assignment:latest
- cd q_assignment
- git pull
- python3 test_q_validator.py
- python3 test_q_processor.py
- python3 assignment.py --file=input.txt 

Or test by building a Docker image using the file "Dockerfile"

Continuous integration:
 - I utilized bitbucket's pipelines, the CI configurations are in the file "bitbucket-pipelines.yml"

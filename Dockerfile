FROM ubuntu:18.10
LABEL description="Image to test the assignment"

RUN apt update && apt install -y python3 && apt install -y git && apt install python3-pip -y


WORKDIR /home

RUN git clone https://longroad4@bitbucket.org/longroad4/q_assignment.git
RUN pip3 install -r q_assignment/requirements.txt
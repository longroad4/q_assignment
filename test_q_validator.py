import unittest
from lib.q_validator import q_validator
import json

class TestQvalidator(unittest.TestCase):


    def test_validate(self):
        v = q_validator(False)

        result = v.validate("{\"event_date\" : \"2018-11-14'T'14:01:45.437-0700\" , \"event_name\" : \"uninstall\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"1.0.0\"}")
        self.assertEqual(result, True)

        # invalid event_date
        result = v.validate("{\"event_date\" : \"2018-11-14'T'1437-0700\" , \"event_name\" : \"uninstall\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"1.0.0\"}")
        self.assertEqual(result, False)

        # Missing event_date
        result = v.validate("{\"event_name\" : \"uninstall\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"1.0.0\"}")
        self.assertEqual(result, False)

        # missing event_name
        result = v.validate("{\"event_date\" : \"2018-11-14'T'14:01:45.437-0700\"  , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"1.0.0\"}")
        self.assertEqual(result, False)

        # missing user_agent
        result = v.validate("{\"event_date\" : \"2018-11-14'T'14:01:45.437-0700\" , \"event_name\" : \"uninstall\" ,\"product_version\" : \"1.0.0\"}")
        self.assertEqual(result, True)

        # missing product_version
        result = v.validate("{\"event_date\" : \"2018-11-14'T'14:01:45.437-0700\" , \"event_name\" : \"uninstall\" }")
        self.assertEqual(result, True)
        
        # invalid user_agent
        result = v.validate("{\"event_date\" : \"2018-11-14'T'14:01:45.437-0700\" , \"event_name\" : \"uninstall\" , \"user_agent\" : \" xyz\" ,\"product_version\" : \"1.0.0\"}")
        self.assertEqual(result, False)

        # invalid product_version
        result = v.validate("{\"event_date\" : \"2018-11-14'T'14:01:45.437-0700\" , \"event_name\" : \"uninstall\" ,\"product_version\" : \"123\"}")
        self.assertEqual(result, False)

        # invalid string
        result = v.validate("xyz")
        self.assertEqual(result, False)

        # invalid string
        result = v.validate("{\"event_date\"")
        self.assertEqual(result, False)

        # invalid string
        result = v.validate("")
        self.assertEqual(result, False)

        # invalid string
        result = v.validate(" ")
        self.assertEqual(result, False)

        # invalid string
        result = v.validate("1")
        self.assertEqual(result, False)

        # invalid string
        result = v.validate("$")
        self.assertEqual(result, False)

        # invalid string
        result = v.validate("{}")
        self.assertEqual(result, False)

        # Have extra fields
        result = v.validate(("{\"event_date\" : \"2018-11-14'T'14:01:45.437-0700\" , \"event_name\" : \"uninstall\" , \"event_extra\" : \"xyz\" }"))
        self.assertEqual(result, True)

if __name__ == "__main__":
    unittest.main()
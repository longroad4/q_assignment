import unittest
import json
from lib.q_processor import q_processor 

class TestQprocessor(unittest.TestCase):

    def test_isMobileUse(self):
        p = q_processor(False, False, "")
        result = p.isMobileUser("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36")
        self.assertEqual(result, False)
        result = p.isMobileUser("Mozilla/5.0 (Linux; U; Android 4.1.1; en-gb; Build/KLP) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30")
        self.assertEqual(result, True)

    def test_isTimeToSlack(self):
        p = q_processor(False, False, "")
        result = p.isTimeToSlack("2018-11-14'T'14:01:22.235-0700", "2018-11-14'T'15:01:22.235-0700", 1)
        self.assertEqual(result, True)

        result = p.isTimeToSlack("2018-11-14'T'14:01:22.235-0700", "2018-11-14'T'14:21:22.235-0700", 1)
        self.assertEqual(result, False)

        result = p.isTimeToSlack("2018-11-14'T'14:01:22.235-0700", "2018-11-13'T'15:01:22.235-0700", 1)
        self.assertEqual(result, False)

        result = p.isTimeToSlack("2018-11-14'T'14:01:22.235-0700", "2018-11-14'T'15:01:22.235-0700", 24)
        self.assertEqual(result, False)

        result = p.isTimeToSlack("2018-11-14'T'14:01:22.235-0700", "2018-11-14'T'15:01:22.235-0700", 24)
        self.assertEqual(result, False)

        result = p.isTimeToSlack("2019-11-14'T'14:01:22.235-0700", "2018-11-14'T'15:01:22.235-0700", 24)
        self.assertEqual(result, False)

    def test_ruleProductUnInstall(self):
        p = q_processor(False, False, "")
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'14:01:45.437-0700\" , \"event_name\" : \"uninstall\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"1.0.0\"}")
        result = p.ruleProductUnInstall(theJSON)
        self.assertEqual(result, True)
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'14:01:45.437-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"1.0.0\"}")
        result = p.ruleProductUnInstall(theJSON)
        self.assertEqual(result, False)

    def test_ruleNewProductUser(self):
        p = q_processor(False, False, "")
        # Test not a new product user event
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'14:01:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"1.0.0\"}")
        result = p.ruleNewProductUser(theJSON)
        self.assertEqual(result, False)
        self.assertEqual(p.cur_new_user_date, None)
        # Test new product user event
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'14:01:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductUser(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_new_user_date, None)
        # Test new product user event after less than an hour
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'14:31:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductUser(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_new_user_date, None)
        # Test new product user event after over an hour
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'15:31:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductUser(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_new_user_date, None)
        # Test new product user event with future day
        theJSON = json.loads("{\"event_date\" : \"2018-11-16'T'15:31:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductUser(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_new_user_date, None)
        # Test new product user event with past day
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'15:31:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductUser(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_new_user_date, None)

    def test_ruleNewProductUser_withSend(self):
        p = q_processor(False, True, "")
        # Test not a new product user event
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'14:01:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"1.0.0\"}")
        result = p.ruleNewProductUser(theJSON)
        self.assertEqual(result, False)
        self.assertEqual(p.cur_new_user_date, None)
        # Test new product user event
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'14:01:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductUser(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_new_user_date, "2018-11-14'T'14:01:22.235-0700")
        # Test new product user event after less than an hour
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'14:31:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductUser(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_new_user_date, "2018-11-14'T'14:01:22.235-0700")
        # Test new product user event after over an hour
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'15:31:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductUser(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_new_user_date, "2018-11-14'T'15:31:22.235-0700")
        # Test new product user event with future day
        theJSON = json.loads("{\"event_date\" : \"2018-11-16'T'15:31:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductUser(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_new_user_date, "2018-11-16'T'15:31:22.235-0700")
        # Test new product user event with past day
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'15:31:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductUser(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_new_user_date, "2018-11-16'T'15:31:22.235-0700")

    def test_ruleOldProductUser(self):
        p = q_processor(False, False, "")
        # Test not an old product user event
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'14:01:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleOldProductUser(theJSON)
        self.assertEqual(result, False)
        self.assertEqual(p.cur_new_user_date, None)
        # Test an old product user event
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'14:01:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"1.0.9\"}")
        result = p.ruleOldProductUser(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_old_user_date, None)
        # Test old product user event after less than 24 hours
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'20:01:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"1.0.9\"}")
        result = p.ruleOldProductUser(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_old_user_date, None)
        # Test old product user event after 24 hours
        theJSON = json.loads("{\"event_date\" : \"2018-11-15'T'14:01:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"1.0.9\"}")
        result = p.ruleOldProductUser(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_old_user_date, None)

    def test_ruleOldProductUser_withSend(self):
        p = q_processor(False, True, "")
        # Test not an old product user event
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'14:01:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleOldProductUser(theJSON)
        self.assertEqual(result, False)
        self.assertEqual(p.cur_new_user_date, None)
        # Test an old product user event
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'14:01:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"1.0.9\"}")
        result = p.ruleOldProductUser(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_old_user_date, "2018-11-14'T'14:01:22.235-0700")
        # Test old product user event after less than 24 hours
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'20:01:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"1.0.9\"}")
        result = p.ruleOldProductUser(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_old_user_date, "2018-11-14'T'14:01:22.235-0700")
        # Test old product user event after 24 hours
        theJSON = json.loads("{\"event_date\" : \"2018-11-15'T'14:01:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"1.0.9\"}")
        result = p.ruleOldProductUser(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_old_user_date, "2018-11-15'T'14:01:22.235-0700")

    def test_ruleNewProductMobile(self):
        p = q_processor(False, False, "")
        # Test not a mobile user event
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'14:01:22.235-0700\" , \"event_name\" : \"uninstall\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductMobile(theJSON)
        self.assertEqual(result, False)
        self.assertEqual(p.cur_mobile_date, None)
        # Test a first mobile user event
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'12:01:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Linux; U; Android 4.1.1; en-gb; Build/KLP) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductMobile(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_mobile_date, None)
        # Test a first mobile user event after less than 1 hour
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'12:59:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Linux; U; Android 4.1.1; en-gb; Build/KLP) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductMobile(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_mobile_date, None)
        # Test a first mobile user event after more than 1 hour
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'13:01:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Linux; U; Android 4.1.1; en-gb; Build/KLP) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductMobile(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_mobile_date, None)

    def test_ruleNewProductMobile_withSend(self):
        p = q_processor(False, True, "")
        # Test not a mobile user event
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'14:01:22.235-0700\" , \"event_name\" : \"uninstall\" , \"user_agent\" : \" Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductMobile(theJSON)
        self.assertEqual(result, False)
        self.assertEqual(p.cur_mobile_date, None)
        # Test a first mobile user event
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'12:01:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Linux; U; Android 4.1.1; en-gb; Build/KLP) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductMobile(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_mobile_date, "2018-11-14'T'12:01:22.235-0700")
        # Test a first mobile user event after less than 1 hour
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'12:59:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Linux; U; Android 4.1.1; en-gb; Build/KLP) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductMobile(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_mobile_date, "2018-11-14'T'12:01:22.235-0700")
        # Test a first mobile user event after more than 1 hour
        theJSON = json.loads("{\"event_date\" : \"2018-11-14'T'13:01:22.235-0700\" , \"event_name\" : \"user_login\" , \"user_agent\" : \" Mozilla/5.0 (Linux; U; Android 4.1.1; en-gb; Build/KLP) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30\" ,\"product_version\" : \"2.0.0\"}")
        result = p.ruleNewProductMobile(theJSON)
        self.assertEqual(result, True)
        self.assertEqual(p.cur_mobile_date, "2018-11-14'T'13:01:22.235-0700")

if __name__ == "__main__":
    unittest.main()
